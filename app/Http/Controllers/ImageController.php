<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;
use test\Mockery\ReturnTypeObjectTypeHint;

class ImageController extends Controller
{
    //Class Variables
    private const SRC = 'images/';


    /**
     * start function will be called when the route have been entered in address bar
     *
     * @param $imageName
     * @param null $height
     * @param null $width
     */
    public function start($imageName , $height = null , $width = null)
    {
        if (!$this->checkImageExistence($imageName))
            return 'Please enter a valid image name...';

        if (!$this->validateHeightAndWidth($height))
            return 'Please enter a height between 50 and 150 or nothing to get image with original height ...';

        if (!$this->validateHeightAndWidth($width))
            return 'Please enter a width between 50 and 150 or nothing to get image with original width ...';

        if ($this->checkResizedImageExistence($imageName , $width , $height))
            return $this->downloadImage('resized-photos/' . $width . '_' . $height . '_' . $imageName);

        // will resize and send image if does not exists and validations go good
        $imageAddress = \Storage::path(self::SRC . $imageName);
        $image = Image::make($imageAddress);

        $image = $this->imageResize($image , $width , $height);

        return $this->imageStorage($imageAddress , $image , $imageName);

    }

    private function checkResizedImageExistence($imageName , $width , $height)
    {
        return \Storage::exists('resized-photos/' . $width . '_' . $height . '_' . $imageName);
    }

    /**
     * save the resized image to storage in /resized-photos/
     * then return the resized photo in order to be downloaded on user's
     *
     * @param $imageAddress
     * @param $imageObj
     * @param $imageName
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    private function imageStorage($imageAddress , $imageObj , $imageName)
    {
        $resizedImageAddress = \Storage::path('resized-photos/');
        $imageObj->save($resizedImageAddress . $imageObj->width() . '_' . $imageObj->height() . '_' . $imageName);
        return \Storage::download('resized-photos/'. $imageObj->width() . '_' . $imageObj->height() . '_' . $imageName);
    }

    //will return the file of the image address
    private function downloadImage($imageAddress)
    {
        return \Storage::download($imageAddress);
    }

    /**
     * @param $imageObj
     * @param $width
     * @param $height
     * @return mixed
     */
    private function imageResize($imageObj ,$width , $height)
    {
        $outputHeight = $imageObj->height();
        $outputWidth = $imageObj->width();

        if ($outputHeight !== $height And $height !== null)
            $outputHeight = $height;

        if ($outputWidth !== $width And $width !== null)
            $outputWidth = $width;

        return $imageObj->resize($outputWidth , $outputHeight);

    }

    /**
     * loadImage will read image from storage and will return the object
     *
     * @param $imageName
     * @return \Intervention\Image\Image
     */
    private function loadImage($imageName)
    {

    }

    /**
     * checks if image exists inside images folder at storage/app/images and returns true if exists and no if not
     *
     * @param $name
     * @return bool
     */
    private function checkImageExistence($name)
    {
        return Storage::exists(self::SRC . $name);
    }


    /**
     * checks if data is number and it is between 50 and 150
     *
     * @param $data
     * @return bool
     */
    private function validateHeightAndWidth($data)
    {
        // checks if is null
        if ($data === null)
            return true;

        //checks if is integer
        if (!is_integer( (int)$data ))
            return false;

        //checks if is greater than 150
        if ($data > 150)
            return false;

        //checks if is less than 50
        if ($data < 50)
            return false;

        return true;
    }
}
