<?php

namespace App\Http\Controllers;

use App\Imports\UsersImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use function foo\func;

class ExcelController extends Controller
{
    // TODO take a look at this : https://docs.laravel-excel.com/3.1/imports/heading-row.html
    // TODO comment 
    public function start() // TODO name of method is not good
    {
        //load from file
        $scores = $this->loadScoresFromFileToArray('scores.xlsx');
        //remove null
        $scores = $this->array_null_filter($scores);
        //avgScore
        $avgScores = $this->avgScoreArrayHandler($scores);
        //save the avgScores to a file
        $fileAddress = $this->saveToFile($avgScores);
        //download the file for user and delete the file
        return $this->downloadFileToUser($fileAddress);
    }

    /**
     * will return the download link to user and then delete the scores.txt file
     *
     * @param $fileAddress
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    private function downloadFileToUser($fileAddress) // TODO this method is not necessary
    {
        return response()->download(\Storage::path($fileAddress))->deleteFileAfterSend();
    }

    /**
     * will save scores to a file in storage and return the address of the file
     *
     * @param $result
     * @return string
     */
    private function saveToFile($result)
    {
        foreach ($result as $studentName => $avg) {
            \Storage::append('scores.txt' , $studentName . ' = ' . $avg);
        }
        return 'scores.txt';
    }

    /**
     * make an array that contains the name of use and the avg score that they have through the input scores
     *
     * @param $array
     * @return array
     */
    private function avgScoreArrayHandler($array)
    {
        $result = array();
        foreach ($array as $item)
        {
            $result[$item[0]] = round($this->avgScore(array_slice($item , 1) ));
        }
        return $result;
    }

    /**
     * will calculate weighted avg through our instruction
     *
     * @param $array
     * @return float|int
     */
    private function avgScore($array)
    {
        return
            ( ($array[0] * 2)
            + ($array[1] * 1)
            + ($array[2] * 3)
            + ($array[3] * 2)
            + ($array[4] * 5)
            + ($array[5] * 2)
            + ($array[6] * 1)
            + ($array[7] * 12) ) / 28; // TODO do not write ratio like this. calculate it dynamically
            // TODO key of scores is important
            // what if order of scores changed
            // what if a new question added
    }

    /**
     * unset null rows from array
     *
     * @param $array
     * @return mixed
     */
    private function array_null_filter($array)
    {
        for ($i = 0 ; $i < count($array) ; $i++) {
            if ($array[$i][0] === null) {
                unset($array[$i]);
            }
        }
        foreach ($array as $index => $data) {
            if (!isset($data[1])) {
                unset($array[$index]);
            }
        }

        return $array;
    }

    /**
     * load the file from storage/app/scores as an array
     *
     * @param $fileName
     * @return mixed
     */
    private function loadScoresFromFileToArray($fileName)
    {
        // TODO $array is not necessary
        $array = Excel::toArray(new UsersImport , \Storage::path('scores/' . $fileName))[0];

        return $array;
    }



}
