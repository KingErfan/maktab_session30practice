<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ExcelController as ExcelController;
use App\Http\Controllers\ImageController as ImageController;

Route::get('/', function () {
    return view('welcome');
});

//Image Handler Route
Route::get('/image/{image_name}/{height?}/{width?}' , [ImageController::class , 'start']) -> where(['image_name' => '[a-zA-Z]+\.(jpg|jpeg|png)']);

//Score.xlsx Handler Route
Route::get('/scores/result/get' , [ExcelController::class , 'start']);

